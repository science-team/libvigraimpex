Source: libvigraimpex
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Andreas Metzler <ametzler@debian.org>,
Section: libs
Priority: optional
Build-Depends:
 cmake (>= 3.12),
 debhelper-compat (= 13),
 libfftw3-dev,
 libhdf5-dev,
 libjpeg-dev,
 libopenexr-dev,
 libpng-dev,
 libtiff-dev,
Build-Depends-Indep:
 doxygen,
 ghostscript,
 texlive-latex-base,
 texlive-latex-extra,
Rules-Requires-Root: no
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/libvigraimpex
Vcs-Git: https://salsa.debian.org/science-team/libvigraimpex.git
Homepage: https://ukoethe.github.io/vigra/

Package: libvigraimpex-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends:
 libfftw3-dev,
 libhdf5-dev,
 libjpeg-dev,
 libpng-dev,
 libtiff-dev,
 libvigraimpex11 (= ${binary:Version}),
 ${misc:Depends},
Suggests:
 libvigraimpex-doc,
Description: development files for the C++ computer vision library
 Vision with Generic Algorithms (VIGRA) is a computer vision library
 that puts its main emphasis on flexible algorithms, because
 algorithms represent the principle know-how of this field. The
 library was consequently built using generic programming as
 introduced by Stepanov and Musser and exemplified in the C++ Standard
 Template Library. By writing a few adapters (image iterators and
 accessors) you can use VIGRA's algorithms on top of your data
 structures, within your environment.
 .
 This package contains the header and development files needed to
 build programs and packages using VIGRA.

Package: libvigraimpex11
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Description: C++ computer vision library
 Vision with Generic Algorithms (VIGRA) is a computer vision library
 that puts its main emphasis on flexible algorithms, because
 algorithms represent the principle know-how of this field. The
 library was consequently built using generic programming as
 introduced by Stepanov and Musser and exemplified in the C++ Standard
 Template Library. By writing a few adapters (image iterators and
 accessors) you can use VIGRA's algorithms on top of your data
 structures, within your environment.

Package: libvigraimpex-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends},
Suggests:
 libvigraimpex-dev,
Description: Documentation for the C++ computer vision library
 Vision with Generic Algorithms (VIGRA) is a computer vision library
 that puts its main emphasis on flexible algorithms, because
 algorithms represent the principle know-how of this field. The
 library was consequently built using generic programming as
 introduced by Stepanov and Musser and exemplified in the C++ Standard
 Template Library. By writing a few adapters (image iterators and
 accessors) you can use VIGRA's algorithms on top of your data
 structures, within your environment.
 .
 This package contains documentation for the VIGRA library.
